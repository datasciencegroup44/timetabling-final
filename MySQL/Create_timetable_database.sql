DROP DATABASE IF EXISTS `timetables`;

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema timetables
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema timetables
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `timetables` DEFAULT CHARACTER SET utf8 ;
USE `timetables` ;

-- -----------------------------------------------------
-- Table `timetables`.`building`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`building` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`room` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `building_id` INT NOT NULL,
  `capacity` INT NULL,
  `url` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_room_building_idx` (`building_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`activity` (
  `id` INT NOT NULL,
  `name` VARCHAR(200) NULL,
  `number_of_students` INT NULL,
  `activity_type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`timeslot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`timeslot` (
  `id` INT NOT NULL,
  `start_time` TIME NULL,
  `end_time` TIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`date`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`date` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `timeslot_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Date_timeslot1_idx` (`timeslot_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`teacher` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `employeenumber` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetables`.`schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `timetables`.`schedule` (
  `room_id` INT NOT NULL,
  `date_id` INT NOT NULL,
  `activity_id` INT NOT NULL,
  `teacher_id` INT NOT NULL,
  INDEX `fk_Schedule_date1_idx` (`date_id` ASC),
  INDEX `fk_schedule_activity1_idx` (`activity_id` ASC),
  INDEX `fk_schedule_teacher1_idx` (`teacher_id` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- Load timeslot data.

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `timeslot` WRITE;
/*!40000 ALTER TABLE `timeslot` DISABLE KEYS */;
INSERT INTO `timeslot` VALUES (0,'00:00:00','00:14:59'),(1,'00:15:00','00:29:59'),(2,'00:30:00','00:44:59'),(3,'00:45:00','00:59:59'),(4,'01:00:00','01:14:59'),(5,'01:15:00','01:29:59'),(6,'01:30:00','01:44:59'),(7,'01:45:00','01:59:59'),(8,'02:00:00','02:14:59'),(9,'02:15:00','02:29:59'),(10,'02:30:00','02:44:59'),(11,'02:45:00','02:59:59'),(12,'03:00:00','03:14:59'),(13,'03:15:00','03:29:59'),(14,'03:30:00','03:44:59'),(15,'03:45:00','03:59:59'),(16,'04:00:00','04:14:59'),(17,'04:15:00','04:29:59'),(18,'04:30:00','04:44:59'),(19,'04:45:00','04:59:59'),(20,'05:00:00','05:14:59'),(21,'05:15:00','05:29:59'),(22,'05:30:00','05:44:59'),(23,'05:45:00','05:59:59'),(24,'06:00:00','06:14:59'),(25,'06:15:00','06:29:59'),(26,'06:30:00','06:44:59'),(27,'06:45:00','06:59:59'),(28,'07:00:00','07:14:59'),(29,'07:15:00','07:29:59'),(30,'07:30:00','07:44:59'),(31,'07:45:00','07:59:59'),(32,'08:00:00','08:14:59'),(33,'08:15:00','08:29:59'),(34,'08:30:00','08:44:59'),(35,'08:45:00','08:59:59'),(36,'09:00:00','09:14:59'),(37,'09:15:00','09:29:59'),(38,'09:30:00','09:44:59'),(39,'09:45:00','09:59:59'),(40,'10:00:00','10:14:59'),(41,'10:15:00','10:29:59'),(42,'10:30:00','10:44:59'),(43,'10:45:00','10:59:59'),(44,'11:00:00','11:14:59'),(45,'11:15:00','11:29:59'),(46,'11:30:00','11:44:59'),(47,'11:45:00','11:59:59'),(48,'12:00:00','12:14:59'),(49,'12:15:00','12:29:59'),(50,'12:30:00','12:44:59'),(51,'12:45:00','12:59:59'),(52,'13:00:00','13:14:59'),(53,'13:15:00','13:29:59'),(54,'13:30:00','13:44:59'),(55,'13:45:00','13:59:59'),(56,'14:00:00','14:14:59'),(57,'14:15:00','14:29:59'),(58,'14:30:00','14:44:59'),(59,'14:45:00','14:59:59'),(60,'15:00:00','15:14:59'),(61,'15:15:00','15:29:59'),(62,'15:30:00','15:44:59'),(63,'15:45:00','15:59:59'),(64,'16:00:00','16:14:59'),(65,'16:15:00','16:29:59'),(66,'16:30:00','16:44:59'),(67,'16:45:00','16:59:59'),(68,'17:00:00','17:14:59'),(69,'17:15:00','17:29:59'),(70,'17:30:00','17:44:59'),(71,'17:45:00','17:59:59'),(72,'18:00:00','18:14:59'),(73,'18:15:00','18:29:59'),(74,'18:30:00','18:44:59'),(75,'18:45:00','18:59:59'),(76,'19:00:00','19:14:59'),(77,'19:15:00','19:29:59'),(78,'19:30:00','19:44:59'),(79,'19:45:00','19:59:59'),(80,'20:00:00','20:14:59'),(81,'20:15:00','20:29:59'),(82,'20:30:00','20:44:59'),(83,'20:45:00','20:59:59'),(84,'21:00:00','21:14:59'),(85,'21:15:00','21:29:59'),(86,'21:30:00','21:44:59'),(87,'21:45:00','21:59:59'),(88,'22:00:00','22:14:59'),(89,'22:15:00','22:29:59'),(90,'22:30:00','22:44:59'),(91,'22:45:00','22:59:59'),(92,'23:00:00','23:14:59'),(93,'23:15:00','23:29:59'),(94,'23:30:00','23:44:59'),(95,'23:45:00','23:59:59');
/*!40000 ALTER TABLE `timeslot` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- -----------------------------------------------------
-- fill dates
-- -----------------------------------------------------

DROP PROCEDURE IF EXISTS filldates;
DELIMITER |
CREATE PROCEDURE filldates(dateStart DATE, dateEnd DATE)
BEGIN
DECLARE i INT;
  WHILE dateStart <= dateEnd DO
	SET i = 0;
    WHILE i < 96 DO
		INSERT INTO date (`date`,timeslot_id) VALUES (dateStart,i);
        SET i = i + 1;
	END WHILE;
    SET dateStart = date_add(dateStart, INTERVAL 1 DAY);
  END WHILE;
END;
|
DELIMITER ;

CALL filldates('2015-08-31','2018-08-31');

-- -----------------------------------------------------
-- Create view without teacher
-- -----------------------------------------------------

CREATE VIEW `without teachers` AS
    SELECT DISTINCT
        `activity`.`name` AS `name`,
        `activity`.`activity_type` AS `activity_type`,
        `building`.`name` AS `building name`,
        `date`.`date` AS `date`,
        `timeslot`.`id` AS `timeslot id`,
        `timeslot`.`start_time` AS `start_time`,
        `timeslot`.`end_time` AS `end_time`,
        `room`.`building_id` AS `building_id`,
        `room`.`name` AS `room name`
    FROM
        (((((`schedule`
        LEFT JOIN `activity` ON ((`schedule`.`activity_id` = `activity`.`id`)))
        LEFT JOIN `room` ON ((`schedule`.`room_id` = `room`.`id`)))
        LEFT JOIN `date` ON ((`schedule`.`date_id` = `date`.`id`)))
        LEFT JOIN `timeslot` ON ((`date`.`timeslot_id` = `timeslot`.`id`)))
        LEFT JOIN `building` ON ((`room`.`building_id` = `building`.`id`)))