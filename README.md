# README #

Welcome to the Data Science Repository of Team 44

### Team Members ###
* Rick Harms  -   j.r.harms@student.utwente.nl
* Jaap Vermeij  -  j.vermeij-1@student.utwente.nl

###About this repository ###

This repository contains all the files needed to setup and run the dashboard. 

### Structure? ###

This repository is structured in different folders.

* Data - Contains all raw data, including the latest Database dump.
* Kettle - Contains the Kettle file to import data from the timetabling API.
* MySQL - Contains the models of the database, ans SQL scripts.
* Tableau - Contains the dashboard and the static data in case no live connection is present.

### Getting started ###
To get started one can just open the "Vizualizing Occupancy Dashboard.twbx" file in the Tableau folder. This self containing file has the latest database.

### Start from scratch ###
The following steps describe how the database can be created and updated.
## Creating database ##
The first step is to create the database. This can be done with relative ease by executing the "Create_timetable_database.sql" file. Currently, the dates from 2015-08-31 till 2018-08-31 are taken into account. Should this period be changed, then simply change the parameters in the 
```
#!sql
CALL filldates('2015-08-31','2018-08-31');
```
function inside "Create_timetable.sql".

## import data from the API ##
Importing data is done with Pentaho Kettle Spoon. open the "Import_From_Ap.ktr" file in the Kettle folder. Before running the procedure, some parameters have to be filled in. ![Parameters_Kettle.PNG](https://bitbucket.org/repo/x8A5MGe/images/3662766619-Parameters_Kettle.PNG)

**API_TOKEN** - This is the API_TOKEN to get access to the timetables API of the university of twente.

**LOCATION_TIMESLOT** - The location of timeslot.csv. If the file is in the same folder as the kettle, then this value can just stay default.

**MYSQL_HOST** - The Address of the MYSQL server. default: localhost.

**MYSQL_PASS** - The password for the MYSQL server.

**MYSQL_USER** - The username for the MYSQL server.

**Mind that this process can take several hours to complete!**

## Tableau Dashboard ##
After the data is stored on the server, tableau can connect to this. simply change the connection to the desired server.